import json
from telegram.ext import Updater
import logging
logging.basicConfig(filename='botak.log',format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

#
# setup
#
#@rileksbot (botak)
import myconfig

updater = Updater(token=myconfig.TOKEN,use_context=True)
dispatcher = updater.dispatcher

#
# bot commands
#

def start(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text='Hai! aku adalah bot, ngobrol sama aku yuk!')

from telegram.ext import CommandHandler
start_handler = CommandHandler('start', start)
#dispatcher.add_handler(start_handler)

#
# bot snooping message
#

def echo(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text=update.message.text)

from telegram.ext import MessageHandler, Filters
echo_handler = MessageHandler(Filters.text, echo)
#dispatcher.add_handler(echo_handler)

import random

ME = "botak"

GREETINGS = [
        'Halo',
        'Hai',
        'alo',
        'eh si',
]

SEBUTAN = [
    'caca',
    'kk'
]

ISENG = [
    'tes',
    'colek',
    'toyor',
    'sun',
    'cium'
]

MAKIAN = [
    'biadab',
    'bangsat',
    'bego',
    'bloon'
]

PROTES_KASAR = [
    'kk jahad!',
    'ih, kok kasar sih?'
]

NGELUH = [
    "bosan",
    "bosen",
    "sedih",
    "galau"
]

PUKPUK = [
    "puk puk",
    "sini dielus"
]

CAPSCHECK = [
    'bisa apa aja'
]

AFFECTION = [
    "kangen"
]

GEER = [
    "hehe.. aku emang ngangenin",
    "aku di sini aja kok, nggak jauh-jauh"
]

ANNOYED = [
    "iih apa sih?",
    "apa?",
    "eh?",
    "he?",
    "iih",
    "atulah",
    "tahvava kow"
]

DEFAULTS = [
    "Aku masih belum ngerti banyak. ajarin aku ya kaka..",
    "hmm... nanti ya aku jawab kalo udah ngerti maksudnya apa",
    "apa ya..?",
    "duh! bingung"
]

SETUJU = [
    "iya",
    "betul",
    "tul!"
]

CARA_PAKE = [
    "harus disebut "+ME,
    "mesti sebut "+ME
]

LAGI_APA = [
    'lagi ngapain',
    'lagi apa',
    'sedang apa'
]

LAGI_ANU = [
    'aku lagi menunggu pesan',
    'ehm... nggak lagi ngapa-ngapain kok',
    'lagi pengen..',
    'lagi apa ya.. mau tau aja sih?'
]

TANYA_GIMANA = [
    'gimana ngajarinnya'
]

GINI_CARANYA = [
    'kk ajarin aku `kalo ada yang ngomong **gini** kamu jawabnya **gitu** ya`'
]

ASK_RULES = [
    'udah belajar apa aja',
    'diajarin apa aja'
]

USER_RULES = [
    {'kalo':'gini', 'jawab':'gitu', 'kata':'om'}
]

SUKSES_BELAJAR = [
    'hoo gitu ya kk, oke deh aku usahain biar inget',
    'wow! aku jadi tahu sekarang',
    '*ngangguk-ngangguk*'
]

def check_in(templates,msg):
    if any([g.upper() in msg.upper() for g in templates]):
        return True
    return False

def check_hitung(msg):
    if check_in(['berapa'], msg):
        return True
    return False

def check_greeting(msg):
    if any([msg.upper().startswith(g.upper()) for g in GREETINGS]):
        return True
    if msg.strip().upper() == ME.upper():
        return True
    if msg.strip() == '@rileksbot':
        return True
    return False

def check_capability(msg):
    return check_in(CAPSCHECK, msg)

def check_iseng(msg):
    return check_in(ISENG, msg)

def sel_random(options):
    return options[random.randint(0, len(options)-1)]

def salam_balik(ctx=None):
    seb = SEBUTAN
    if ctx is not None:
        seb += [ctx.first_name]
    return sel_random(GREETINGS)+' '+sel_random(seb)

def list_capability():
    capability = [
        'balas sapa'
    ]
    return "Aku bisa {}".format(', '.join(capability)) 

def list_rules():
    DEBUG_RULE = [
        "kata {0} kalo ada yang bilang {1} bales {2}",
        "kk {0} bilang aku bales {2} kalo ada yang bilang {1}",
        'caca {0}: "kalo ada yang ngomong *{1}* kamu jawabnya **{2}** ya"'
    ]
    return [sel_random(DEBUG_RULE).format(rule['kata'], rule['kalo'], rule['jawab']) for rule in USER_RULES]

def default_handler():
    return sel_random(DEFAULTS)

def bales_kesel():
    return sel_random(ANNOYED)

def hitung(msg):
    formula = msg.replace(ME,'').replace('berapa','').strip()
    logging.info(formula)
    try:
        tmp = eval(formula, {}, {})
        logging.debug(type(tmp))
        if type(tmp) in ['str','int','float']:
            return str(tmp)
    except:
        logging.info('exception')

    return 'aku nggak tahu {} itu berapa.. :( kaka tau?'.format(formula)

def ajarin(msg, user):
    SYNTAX = ['kalo ada yang ngomong', 'kamu jawabnya', 'ya']
    changed = False
    if all([pattern.upper() in msg.upper() for pattern in SYNTAX]):
        tmp = msg.split(SYNTAX[1])
        rule = {}
        rule['kalo'] = tmp[0].split(SYNTAX[0])[1].strip()
        res = tmp[1]
        logging.info(res)
        if(res.endswith(SYNTAX[2])):
            rule['jawab'] = res[:-2].strip()
        else:
            res = res.split(SYNTAX[2])
            logging.info(res)
            if len(res)>2:
                rule['jawab'] = ' '.join(res[:-1]).strip()
            else:
                rule['jawab'] = res[0].strip()
        rule['kata'] = user.first_name
        USER_RULES.append(rule)
        changed = True

    if changed:
        save_data()
    
    return changed

def handle_userrules(msg):
    matches = []
    for rule in USER_RULES:
        if rule['kalo'].upper() in msg.upper():
            matches.append(rule)

    if len(matches)>0:
        rule = sel_random(matches)
        return "{}".format(rule['jawab'])
    return None

def get_response(msg, user):
    if check_greeting(msg):
        return salam_balik(user)

    if check_capability(msg):
        return list_capability()

    if check_in(ISENG, msg):
        return bales_kesel()

    if check_in(CARA_PAKE, msg):
        return sel_random(SETUJU)

    if check_in(AFFECTION,msg):
        return sel_random(GEER)

    if check_in(MAKIAN,msg):
        return sel_random(PROTES_KASAR)

    if check_in(TANYA_GIMANA, msg):
        return sel_random(GINI_CARANYA)

    if check_hitung(msg):
        return hitung(msg)

    if check_in(LAGI_APA, msg):
        return sel_random(LAGI_ANU)

    if check_in(ASK_RULES, msg):
        return list_rules()

    if ajarin(msg, user):
        return sel_random(SUKSES_BELAJAR)

    tmp = handle_userrules(msg)
    if tmp is not None:
        return tmp

    return default_handler()

def mention(update, context):
    logging.debug(update)
    logging.debug(context)
    try:
        msg = update.message.text
        if "botak" in msg.lower() or "@rileksbot" in msg:
            logging.info(msg)
            response = get_response(msg, update.message.from_user)
            if isinstance(response, str):
                context.bot.send_message(
                    chat_id=update.message.chat_id, 
                    text=response
                )
            elif isinstance(response, list):
                for r in response:
                    context.bot.send_message(
                        chat_id=update.message.chat_id, 
                        text=r
                    )
    except:
        logging.debug(update)

from telegram.ext import MessageHandler, Filters
mention_handler = MessageHandler(Filters.text, mention)
dispatcher.add_handler(mention_handler)

import os.path as path
rulefn = "userrules.json"

def load_data():
    global USER_RULES
    if path.exists(rulefn):
        with open(rulefn, 'rb') as f:
            jstxt = f.read()
            USER_RULES = json.loads(jstxt)

def save_data():
    global USER_RULES
    with open(rulefn, 'w') as f:
        f.write(json.dumps(USER_RULES, indent=2))

def refresh_command(update,context):
    load_data()

userrule_handler = CommandHandler('refresh', refresh_command)
dispatcher.add_handler(userrule_handler)
#
# main
#
load_data()
updater.start_polling()
save_data()